package com.example.airportapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.airportapplication.Constants;
import com.example.airportapplication.Model.TimeTable;
import com.example.airportapplication.R;
import com.example.airportapplication.Service.Formater;

import java.util.Arrays;
import java.util.List;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.TimeTableHolder> {

    private  List<TimeTable> timeTables;
    private Context ctx;

    public  TimeTableAdapter(List<TimeTable> timeTables, Context ctx){

        this.ctx=ctx;
        this.timeTables=timeTables;
    }
    @NonNull
    @Override
    public TimeTableHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.airport_list_item,viewGroup,false);
        return new TimeTableHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeTableHolder timeTableHolder, int i) {



        final TimeTable timeTable=timeTables.get(i);




    timeTableHolder.txtTime.setText(Formater.formatTime(timeTable.getDeparture().getScheduledTime()));
        timeTableHolder.txtStatus.setText(Formater.checkNull(timeTable.getStatus()));
       timeTableHolder.txtAirlinename.setText(Formater.checkNull(timeTable.getAirline().getName()));
        timeTableHolder.txtFlightNumber.setText(Formater.checkNull(timeTable.getFlight().getIcaoNumber()));
        timeTableHolder.txtDestination.setText(Formater.checkNull(timeTable.getDestination()));

if(Arrays.asList(Constants.Arrays.GREEN_STATUS).indexOf(Formater.checkNull(timeTable.getStatus()))!=-1) {
    timeTableHolder.imgStatus.setImageResource(R.drawable.green_dot_x1);
}else{

    timeTableHolder.imgStatus.setImageResource(R.drawable.red_dot_x1);
}
    }


    @Override
    public int getItemCount() {
        return timeTables.size();
    }


    public class TimeTableHolder extends RecyclerView.ViewHolder {

        TextView  txtTime,txtStatus,txtAirlinename,txtDestination,txtFlightNumber;
        ImageView imgStatus,imgPlane;

        public TimeTableHolder(@NonNull View itemView) {
            super(itemView);
txtTime=itemView.findViewById(R.id.tvTime);
txtStatus=itemView.findViewById(R.id.tvStatus);
txtAirlinename=itemView.findViewById(R.id.tvAirlineName);
txtDestination=itemView.findViewById(R.id.tvDestination);
txtFlightNumber=itemView.findViewById(R.id.tvAirlineNumber);

imgStatus=itemView.findViewById(R.id.imgStatus);

        }
    }
}
