package com.example.airportapplication.Service;

import com.example.airportapplication.Model.Cities;
import com.example.airportapplication.Model.NearbyAirport;
import com.example.airportapplication.Model.TimeTable;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetAirports {





    @GET("nearby?key=28e906-c2619e")
 Call<List<NearbyAirport>>getAllNearbyAairports(@Query("lat") String lat, @Query("lng") String lng, @Query("distance") double distance);



    @GET("timetable?key=28e906-c2619e")
    Call< List<TimeTable> > getTimetable(@Query("iataCode") String iataCode, @Query("type") String Type);


    @GET("cityDatabase?key=28e906-c2619e")
    Call< List<Cities> > getCities(@Query("codeIataCity") String codeIataCity);


}
