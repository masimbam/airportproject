package com.example.airportapplication.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formater {


    public static String formatTime(String dateTime){


        if(dateTime==null)dateTime=new Date().toString();


        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
try {
    Date d = dateFormat.parse(dateTime);

    SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm");

    return sdfDate.format(d);

}catch (ParseException e){

    return "00:00";
}


    }

    public static String checkNull(String data){

        if(data==null){

            return "Information Not Provided";
        }else{

            return data;
        }
    }
}
