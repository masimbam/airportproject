package com.example.airportapplication;

import java.net.FileNameMap;

public interface Constants {

    interface Generic{
        final int DISTANCE=400;
        final String CURRENT_LOCATION_POSITION="HERE";

        final String CURRENT_LOCATION_TITLE=" I AM HERE";

        final  int DEFAULT_ZOOM_LEVEL=10;

        final String TIMETABLE_ID="TIMETABLEOBJECT";

        final String DEPARTURE="departure";
        final String DESTINATION="destination";

        final String FAILED="SERVICE CURRENTLY UNAVAILABLE ";

        final int RADIUS_OVERLAY=100000;
        final int STROKE_WIDTH_OVERLAY=3;
        final int REQUEST_CODE = 101;


    }

    interface  Arrays{

        final  String[] GREEN_STATUS={"landed","scheduled","active"};

        final  String[] RED_STATUS={"cancelled","incident","diverted","redirected","unknown"};
    }
}
