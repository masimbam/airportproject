package com.example.airportapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.airportapplication.Adapter.TimeTableAdapter;
import com.example.airportapplication.Model.Cities;
import com.example.airportapplication.Model.NearbyAirport;
import com.example.airportapplication.Model.TimeTable;
import com.example.airportapplication.Service.Formater;
import com.example.airportapplication.Service.GetAirports;
import com.example.airportapplication.Service.RetrofitClient;


import java.util.List;

import io.netopen.hotbitmapgg.library.view.RingProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeTables extends AppCompatActivity {

    NearbyAirport nearbyAirport;

    TimeTableAdapter timeTableAdapter;

    RecyclerView recyclerView;

    TextView airportName;
    RingProgressBar mRingProgressBar;
    TextView airportLocation;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Connecting");
        progress.setMessage("Retrieving Schedules...");
        progress.show();

        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progress.cancel();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 2000);
        setContentView(R.layout.activity_time_tables);


        nearbyAirport=(NearbyAirport)getIntent().getSerializableExtra(Constants.Generic.TIMETABLE_ID) ;
recyclerView=findViewById(R.id.recyclerView);

airportName=findViewById(R.id.txtAirport);

        airportLocation=findViewById(R.id.txtAirportLocation);

        airportName.setText(Formater.checkNull(nearbyAirport.getNameAirport()));
        airportLocation.setText(Formater.checkNull(nearbyAirport.getCodeIataCity()));
        final GetAirports service = RetrofitClient.getRetrofitInstance().create(GetAirports.class);

        Call<List<TimeTable>> call=   service.getTimetable(Formater.checkNull(nearbyAirport.getCodeIataAirport()), Constants.Generic.DEPARTURE);

        call.enqueue(new Callback<List<TimeTable>>() {
            @Override
            public void onResponse(Call<List<TimeTable>> call, Response<List<TimeTable>> response) {


                Log.d("TIMETABLES",response.body().get(0).getStatus());

              List<TimeTable> timetable=  setCities(response.body(),service);
                timeTableAdapter=new TimeTableAdapter(timetable,TimeTables.this);

                recyclerView.setAdapter(timeTableAdapter);
            }

            @Override
            public void onFailure(Call<List<TimeTable>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), Constants.Generic.FAILED , Toast.LENGTH_LONG).show();

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        Log.d("SERIALIZED...",nearbyAirport.getNameAirport());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(myIntent);

            }
        });
    }

    private List setCities(final List<TimeTable> body, GetAirports service) {

        for(int i=0;i<body.size();i++) {

            final  TimeTable timeTable=body.get(i);
            Call<List<Cities>> call = service.getCities(body.get(i).getArrival().getIataCode());
            call.enqueue(new Callback<List<Cities>>() {
                @Override
                public void onResponse(Call<List<Cities>> call, Response<List<Cities>> response) {

                    if(response.body().size()>0){

                        timeTable.setDestination(response.body().get(0).getNameCity());
                    }else{
                      timeTable.setDestination(Constants.Generic.DESTINATION);

                    }

                }

                @Override
                public void onFailure(Call<List<Cities>> call, Throwable t) {
                    timeTable.setDestination(Constants.Generic.DESTINATION);
                }
            });


        }
        return body;

    }

}
