package com.example.airportapplication.Model;

import java.io.Serializable;

public class Codeshared implements Serializable {

    private Airline airline;
    private Flight flight;

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
