package com.example.airportapplication.Model;

import java.io.Serializable;

public class Cities implements Serializable {

 private String   cityId;
      private String      nameCity;
      private String codeIataCity;
      private String codeIso2Country;
       private String     latitudeCity;
          private String  longitudeCity;
          private String  timezone;
           private String GMT;
           private String geonameId;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    public String getCodeIataCity() {
        return codeIataCity;
    }

    public void setCodeIataCity(String codeIataCity) {
        this.codeIataCity = codeIataCity;
    }

    public String getCodeIso2Country() {
        return codeIso2Country;
    }

    public void setCodeIso2Country(String codeIso2Country) {
        this.codeIso2Country = codeIso2Country;
    }

    public String getLatitudeCity() {
        return latitudeCity;
    }

    public void setLatitudeCity(String latitudeCity) {
        this.latitudeCity = latitudeCity;
    }

    public String getLongitudeCity() {
        return longitudeCity;
    }

    public void setLongitudeCity(String longitudeCity) {
        this.longitudeCity = longitudeCity;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getGMT() {
        return GMT;
    }

    public void setGMT(String GMT) {
        this.GMT = GMT;
    }

    public String getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(String geonameId) {
        this.geonameId = geonameId;
    }
}
