package com.example.airportapplication.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NearbyAirport  implements Serializable {

  /*  { "nameAirport": "Harare", "codeIataAirport": "HRE", "codeIcaoAirport": "FVHA",
            "latitudeAirport": "-17.91863", "longitudeAirport": "31.099249",
            "timezone": "Africa/Harare",
            "GMT": "2",
            "phone": "",
            "nameCountry": "Zimbabwe", "codeIso2Country": "ZW", "codeIataCity": "HRE", "distance": "12.768585668571678" }*/
  @SerializedName("nameAirport")

            private  String nameAirport;
    @SerializedName("codeIataAirport")

    private  String codeIataAirport;
    @SerializedName("codeIcaoAirport")

    private String codeIcaoAirport;
    @SerializedName("latitudeAirport")

    private String latitudeAirport;

    @SerializedName("longitudeAirport")

    private String longitudeAirport;
    @SerializedName("timezone")

    private String timezone;
    @SerializedName("GMT")

    private String GMT;
    @SerializedName("phone")

    private String phone;
    @SerializedName("nameCountry")

    private String nameCountry;
    @SerializedName("codeIso2Country")
    private String codeIso2Country;
    @SerializedName("codeIataCity")

    private String codeIataCity;
    @SerializedName("distance")

    private String distance;

    public String getCodeIataAirport() {
        return codeIataAirport;
    }

    public void setCodeIataAirport(String codeIataAirport) {
        this.codeIataAirport = codeIataAirport;
    }

    public String getCodeIcaoAirport() {
        return codeIcaoAirport;
    }

    public void setCodeIcaoAirport(String codeIcaoAirport) {
        this.codeIcaoAirport = codeIcaoAirport;
    }

    public String getLatitudeAirport() {
        return latitudeAirport;
    }

    public void setLatitudeAirport(String latitudeAirport) {
        this.latitudeAirport = latitudeAirport;
    }

    public String getLongitudeAirport() {
        return longitudeAirport;
    }

    public void setLongitudeAirport(String longitudeAirport) {
        this.longitudeAirport = longitudeAirport;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getGMT() {
        return GMT;
    }

    public void setGMT(String GMT) {
        this.GMT = GMT;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getCodeIso2Country() {
        return codeIso2Country;
    }

    public void setCodeIso2Country(String codeIso2Country) {
        this.codeIso2Country = codeIso2Country;
    }

    public String getCodeIataCity() {
        return codeIataCity;
    }

    public void setCodeIataCity(String codeIataCity) {
        this.codeIataCity = codeIataCity;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getNameAirport() {
        return nameAirport;
    }

    public void setNameAirport(String nameAirport) {
        this.nameAirport = nameAirport;
    }
}
