package com.example.airportapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.example.airportapplication.Model.NearbyAirport;
import com.example.airportapplication.Service.GetAirports;
import com.example.airportapplication.Service.RetrofitClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback {


    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;


    private static List<NearbyAirport> nearbyAirports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        fetchLastLocation();
    }

    private void fetchLastLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{

           Manifest.permission.ACCESS_FINE_LOCATION
            },Constants.Generic.REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){

                    currentLocation=location;
                 //   Toast.makeText(getApplicationContext(),currentLocation.getLatitude()+".........................."+currentLocation.getLongitude(),Toast.LENGTH_SHORT).show();
                    SupportMapFragment supportMapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.google_map);
                    supportMapFragment.getMapAsync(MainActivity.this);



                }
            }
        });
    }

    Circle circle;

    private GoogleMap mMap;

    List<NearbyAirport> nearbyAirportsPositions=new ArrayList<>();
    @Override
    public void onMapReady(final GoogleMap googleMap) {

        mMap= googleMap;
        LatLng latLng=new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());



        MarkerOptions markerOptions=new MarkerOptions().position(latLng).title(Constants.Generic.CURRENT_LOCATION_TITLE);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,Constants.Generic.DEFAULT_ZOOM_LEVEL));
     Marker myposition=   googleMap.addMarker(markerOptions);
        myposition.setTag(Constants.Generic.CURRENT_LOCATION_POSITION);
        drawCircle(latLng,googleMap);

        GetAirports service = RetrofitClient.getRetrofitInstance().create(GetAirports.class);


        Call<List<NearbyAirport>> call=   service.getAllNearbyAairports(currentLocation.getLatitude()+"",currentLocation.getLongitude()+"",Constants.Generic.DISTANCE);

        call.enqueue(new Callback<List<NearbyAirport>>() {
            @Override
            public void onResponse(Call<List<NearbyAirport>> call, Response<List<NearbyAirport>> response) {

                Log.d("RESPONSE",response.body().get(0).getNameAirport());

                for(int i=0;i<response.body().size();i++){


                    nearbyAirportsPositions.add(response.body().get(i));
                Marker marker=    mMap.addMarker(
                            new MarkerOptions().
                                    position(
                                            new LatLng(Double.parseDouble(response.body().get(i).getLatitudeAirport())
                                                    ,Double.parseDouble(response.body().get(i).getLongitudeAirport()))).
                                    title(response.body().get(i).getNameAirport())


                    );
                marker.setTag(i+"");




                }

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
String position=(String)(marker.getTag());
                        NearbyAirport myAirport=null;
if(position.equals(Constants.Generic.CURRENT_LOCATION_POSITION)){
return  false;

}else{

     myAirport= nearbyAirportsPositions.get(Integer.parseInt(position));
}



                        Intent intent=new Intent(getApplicationContext(),TimeTables.class);

                        intent.putExtra(Constants.Generic.TIMETABLE_ID,myAirport);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        return  true;

                    }
                });

            }

            @Override
            public void onFailure(Call<List<NearbyAirport>> call, Throwable t) {
                Toast.makeText(MainActivity.this, Constants.Generic.FAILED , Toast.LENGTH_LONG).show();

            }
        });



    }

    private Circle drawCircle(LatLng latLng,GoogleMap googleMap) {

        CircleOptions circleOptions=new CircleOptions().center(latLng).radius(Constants.Generic.RADIUS_OVERLAY).fillColor(0x33FF0000).strokeColor(Color.BLUE).strokeWidth(Constants.Generic.STROKE_WIDTH_OVERLAY);

        return googleMap.addCircle(circleOptions);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case Constants.Generic.REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
fetchLastLocation();

                }
                break;

        }
    }
}
